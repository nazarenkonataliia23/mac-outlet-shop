$(document).ready(function(){
    $('.slider').slick({
        // dots:true,
        speed: 1000,
        autoplay: true,
        autoplaySpeed:1500,
        pauseOnFocus: false,
        waitForAnimate: false,
        fade: true,
    });
});

const cEl = (tag, options) => {
    const el = document.createElement(tag)
    Object.assign(el, options)
    return el;
}
const gEl = (selector) => {
    return document.querySelector(selector)
}

class Cards{
    constructor(){
        this.blocks = gEl('.blocks')
        this.createCards();
    }
    _createCard(data){
        const card = cEl('div');

        // card.onclick = () => console.log('card', data)
        card.className = 'block';

        card.innerHTML = `
            <div class="main_info_product">
                <img class="product_img" src="img/${data.imgUrl}">
                <h1>${data.name}</h1>
                <div class="left_in_stock">
                <div class="check"><img src="img/icons/check.svg"></div>
                <div class="col_in_stock"><b>&nbsp ${data.orderInfo.inStock} &nbsp</b></div> left in stock
            </div>
            <div class="price">Price:<b>&nbsp ${data.price} &nbsp</b> $</div>
                <div class="btn_container"></div>
            </div>
            <div class="footer_card">
                <div class="likes"><img src="img/icons/like_filled.svg"></div>
                <div class="percent">
                    <div class="percent_reviews"><b>${data.orderInfo.reviews}%</b>&nbsp Positive reviews</br>Above avarage</div>
                </div>
                <div class="orders"><div class="col_orders"><b>&nbsp ${Math.floor(Math.random()*1000)} &nbsp</b></div>orders</div>
            </div>
        `

        const favorites = cEl('button');
        favorites.className = 'like'
        favorites.onclick = () => console.log(data)
        card.prepend(favorites);

        const btnAddToCart = cEl('button')
        btnAddToCart.className = 'add_to_cart'
        btnAddToCart.innerText = 'Add to cart'
        btnAddToCart.onclick = () => console.log(data)
        card.querySelector('.btn_container').appendChild(btnAddToCart)

        return card;
    }
    createCards(arr=items){
        this.blocks.innerHTML = '';
        const cards = arr.map(data => this._createCard(data))
        this.blocks.append(...cards);
    }
}
const render = new Cards();



class SlickSlider{
    constructor(){
        this.slider = gEl('.slider')
        this.createBanners();
    }
    _createBanner(info){
        const banner = cEl('div');

        banner.className = 'slider_item';

        banner.innerHTML = `
        <img src="${info.imgUrl}" alt="">
        <h1 class="name_banners" style="color: ${info.fontColor}; left: ${info.nameLeftPosition}" >${info.name}</h1>
        <div class="btn-container"></div>
        `
        const btnAddToBanner = cEl('button')
        btnAddToBanner.className = 'add_to_cart'
        btnAddToBanner.innerText = 'Add to cart'
        btnAddToBanner.onclick = () => console.log(info)
        banner.querySelector('.btn-container').appendChild(btnAddToBanner)

        return banner;
    }

    createBanners(arr=picture){
        this.slider.innerHTML = '';
        const banners = arr.map(info => this._createBanner(info))
        this.slider.append(...banners);
    }
}
const renderSlider = new SlickSlider();
